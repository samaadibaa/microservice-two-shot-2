# Wardrobify

Team:

- Tifa - Hats
- Sama Younis - Shoes

## Design

- Home page: a simple welcome page, with two buttons which allows
  user to access to either create a new hat form, or create new
  shoes form.
- Hat form: allows user to create a new hat and choose a closet location
- Shoe form: allows user to create new shoes and choose a bin location
- Nav bar: includes link to view Home, Shoes, Create shoes, and Hats
- Shoes page: list all of the shoes user create with their details
- Hats page: list all of the hats user create with their details

## Shoes microservice

I set up the necessary models and encoders, allowing me to create API
views using the standard HTTP GET, POST, and DELETE request methods.
These views handled shoe details and lists, making it easy to manage them.
I designed several models, like BinVO and Shoe, which were connected
through Foreign Key properties to establish relationships between them.
To prepare for the API views, I created encoders for BinVO and ShoeDetails.
I also created URL paths for shoes, which were added to the URLs file.
Once I had the models, views, and URLs set up, I focused on the frontend.
I developed the necessary forms and lists for the web application and
added navigation links to Nav.js. I also set up routes in App.js to
ensure smooth user navigation.To complete the backend design, I added
the appropriate paths for these views in the URLs file. Once everything
was working smoothly, I moved on to the frontend part of the project.
For the frontend, I started by creating forms and lists required for
the web application. I also added navigation links to Nav.js and set
up routes in App.js to improve user navigation.

## Hats microservice

This microservice allows user to manage their hat collections.
User can create a new hat with its details as: name, color, style,
fabric, picture, as well as choosing a closet location. The location
data is being polled from the Wardrobe microservice, which grabs the
location's name and href. The Hats microservice also allows user to
view a list of all of their hats that they created with their details.
