from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import Hat, LocationVO


class LocationVoDetailEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "closet_name",
        "import_href",
    ]


class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "name",
    ]

    def get_extra_data(self, o):
        return {"location": o.location.closet_name}


class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "name",
        "style",
        "fabric",
        "color",
        "picture_url",
        "location",
    ]
    encoders = {
        "location": LocationVoDetailEncoder(),
    }

    def get_extra_data(self, o):
        return {"id": o.id}


@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    """
    Collection RESTful API handler for all Hat objects.

    GET:
    Returns a dictionary with key "hats",
    with values equal a list of the hat's name,
    location and href.
    {
        "hats": [
            {
                "href": URL to the hat,
                "name": hat's name,
                "location": hat's location (closet name)
            },
            ...
        ]
    }

    POST:
    Create a hat and returns its details.
    {
        "name": hat's name,
        "style": hat's style,
        "fabric": hat's fabric type,
        "color": hat's color,
        "picture_url": hat's picture URL,
        "location": closet href (ex: /api/locations/<id>/")
    }
    """
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "invalid closet href"},
                status=400,
            )
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )



@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_hat(request, pk):
    """
    Single-object API for Hat object.

    GET:
    Returns details of a hat based on its pk value.
    {
        "href": hat's url,
        "name": hat's name,
        "style": hat's style,
        "fabric": hat's fabric type,
        "color": hat's color,
        "picture_url": hat's picture URL,
        "location": {
            "closet_name": closet's name,
            "import_href": closet's url,
        }
    }

    DELETE:
    Deletes a hat object from the application.

    PUT:
    Updates hat's details.
    {
        "name": hat's name,
        "style": hat's style,
        "fabric": hat's fabric type,
        "color": hat's color,
        "picture_url": hat's picture URL,
        "location": closet href (ex: /api/locations/<id>/")
    }
    """
    if request.method == "GET":
        try:
            hat = Hat.objects.get(id=pk)
            return JsonResponse(
                {"hat": hat},
                HatDetailEncoder,
                safe=False,
            )
        except Hat.DoesNotExist:
            return JsonResponse(
                {"message": "this hat does not exist"},
                status=400,
            )
    elif request.method == "DELETE":
        try:
            count, _ = Hat.objects.get(id=pk).delete()
            return JsonResponse({"deleted": count > 0})
        except Hat.DoesNotExist:
            return JsonResponse(
                {"message": "this hat does not exist"}
            )
    else:
        try:
            content = json.loads(request.body)
            hat = Hat.objects.get(id=pk)
            if "location" in content:
                try:
                    location_href = content["location"]
                    location = LocationVO.objects.get(import_href=location_href)
                    content["location"] = location
                except LocationVO.DoesNotExist:
                    return JsonResponse(
                        {"message": "invalid closet href"},
                        status=400,
                        )
                Hat.objects.filter(id=pk).update(**content)
                hat = Hat.objects.get(id=pk)
                return JsonResponse(
                    hat,
                    encoder=HatDetailEncoder,
                    safe=False,
                )
        except Hat.DoesNotExist:
            return JsonResponse(
                {"message": "this hat does not exist"},
                status=400,
            )


@require_http_methods(["GET"])
def api_list_location_vo(request):
    if request.method == "GET":
        location_vo = LocationVO.objects.all()
        return JsonResponse(
            {"location": location_vo},
            encoder=LocationVoDetailEncoder,
            safe=False,
        )
