import React, {useState, useEffect} from 'react';

function ShoesForm () {
    const [bins, setBins] = useState([]);


    //Form data
    const [manufacturer, setManufacturer] = useState('');

    const [modelName, setModelName] = useState('');

    const [color, setColor] = useState('');

    const [pictureUrl, setPictureUrl] = useState('');

    const [bin, setBin] = useState('');

    //Fetching the data of location so we can use them as options(getting it externally)
    const fetchData = async () => {
        const url = "http://localhost:8100/api/bins/"

        const response = await fetch(url);


        if (response.ok) {
            const data = await response.json();


            //set the locations to the list of locations we got from the external api
            setBins(data.bins);

        }
    }


    useEffect(() => {
        fetchData();
    }, []);


    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.manufacturer = manufacturer;
        data.model_name = modelName;
        data.color = color;
        data.picture_url = pictureUrl;
        data.bin = bin;

        const shoesUrl = 'http://localhost:8080/api/shoes/';

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(shoesUrl, fetchConfig);

        if (response.ok) {
          const newShoe = await response.json();
          console.log(newShoe)



          setManufacturer('');
          setModelName('');
          setColor('');
          setPictureUrl('');
          setBin('');
        }
    }


    const handleManufaturerChange = (event) => {
      const value = event.target.value;
      setManufacturer(value);

    }

    const handleModelNameChange = (event) => {
      const value = event.target.value;
      setModelName(value);
    }

    const handleColorChange = (event) => {
      const value = event.target.value;
      setColor(value);
    }



    const handlePictureUrlChange = (event) => {
      const value = event.target.value;
      setPictureUrl(value);
    }

    const handleBinChange = (event) => {
        const value = event.target.value;
        setBin(value);
      }


    return (

        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new shoe</h1>
            <form onSubmit={handleSubmit} id="create-shoes-form">


              <div className="form-floating mb-3">
                <input value={manufacturer} onChange={handleManufaturerChange} placeholder="manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                <label htmlFor="name">Name</label>
              </div>


              <div className="form-floating mb-3">
                <input value={modelName} onChange={handleModelNameChange} placeholder="model_name" required type="text" name="model_name" id="model_name" className="form-control" />
                <label htmlFor="fabric">Fabric</label>
              </div>


              <div className="form-floating mb-3">
                <input value={color} onChange={handleColorChange} placeholder="color" required type="text" name="color" id="color" className="form-control" />
                <label htmlFor="color">Color</label>
              </div>

              <div className="form-floating mb-3">
                <input value={pictureUrl} onChange={handlePictureUrlChange} placeholder="picture_url" required type="text" name="picture_url" id="picture_url" className="form-control" />
                <label htmlFor="color">Color</label>
              </div>

              <div className="mb-3">
                <select value={bin} onChange={handleBinChange} required name="bin" id="bin"  className="form-select">
                  <option value="">Choose a Bin</option>
                  {bins.map(bin => {
                    return (
                      <option key={bin.id} value={bin.id}>
                        {bin.name} Closet {bin.closet_name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>


    );

}

export default ShoesForm;
