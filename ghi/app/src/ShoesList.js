import React, { useState, useEffect } from 'react';

const ShoesList = () => {
  const [shoes, setShoes] = useState([]);

  const getData = async () => {
    try {
      const response = await fetch('http://localhost:8080/api/shoes/');
      if (response.ok) {
        const data = await response.json();
        setShoes(data.shoes);
      } else {
        console.error('Error:', response.status);
      }
    } catch (error) {
      console.error('Error:', error);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  const handleDelete = async (e) => {
    const shoeId = e.target.id;
    const url = `http://localhost:8080/api/shoes/${shoeId}`;

    const fetchConfig = {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
      },
    };

    try {
      const response = await fetch(url, fetchConfig);

      if (response.ok) {
        getData();
      } else {
        console.error('Error:', response.status);
      }
    } catch (error) {
      console.error('Error:', error);
    }
  };

  return (
    <div className="offset-2 col-8 bg-info">
      <div className="shadow p-4 mt-4">
        <div className="flex justify-content-center">
          <h1 className="text-center text-white">A list of our Shoes</h1>
        </div>
        <table className="table">
          <thead>
            <tr>
              <th className="text-center">Manufacturer</th>
              <th className="text-center">Model</th>
              <th className="text-center">Color</th>
              <th className="text-center">Picture</th>
              <th className="text-center">Bin</th>
              <th className="text-center">Delete</th>
            </tr>
          </thead>
          <tbody>
            {shoes.map((shoe, index) => {
              return (
                <tr className="bg-light" key={index}>
                  <td className="text-center">{shoe.manufacturer}</td>
                  <td className="text-center">{shoe.model_name}</td>
                  <td className="text-center">{shoe.color}</td>
                  <td className="text-center">
                    <img src={shoe.picture_url} width={150} height={100} alt={`Shoe ${index}`} />
                  </td>
                  <td className="text-center">{shoe.bin.closet_name}</td>
                  <td className="text-center">
                    <button onClick={handleDelete} id={shoe.id} className="btn btn-danger">
                      Delete
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default ShoesList;
