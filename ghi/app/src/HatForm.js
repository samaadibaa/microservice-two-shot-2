import React from "react";
import { useState, useEffect } from "react";
import { Link } from "react-router-dom";

function HatForm() {
  // fetch closet locations
  const [locations, setLocations] = useState([]);
  const fetchData = async () => {
    const url = "http://localhost:8090/api/location_vo/";

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();

      console.log(data.location);
      setLocations(data.location);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  // create handlers for all fields
  const [name, setName] = useState("");
  const [style, setStyle] = useState("");
  const [fabric, setFabric] = useState("");
  const [color, setColor] = useState("");
  const [pictureUrl, setPictureUrl] = useState("");
  const [location, setLocation] = useState("");

  const handleName = (event) => {
    setName(event.target.value);
  };

  const handleStyle = (event) => {
    setStyle(event.target.value);
  };

  const handleFabric = (event) => {
    setFabric(event.target.value);
  };

  const handleColor = (event) => {
    setColor(event.target.value);
  };

  const handlePictureUrl = (event) => {
    setPictureUrl(event.target.value);
  };

  const handleLocation = (event) => {
    setLocation(event.target.value);
  };

  // handle submit
  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};

    data.name = name;
    data.style = style;
    data.fabric = fabric;
    data.color = color;
    data.picture_url = pictureUrl;
    data.location = location;

    // POST
    const hatUrl = "http://localhost:8090/api/hats/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(hatUrl, fetchConfig);

    if (response.ok) {
      const newHat = await response.json();

      // reset form to empty
      setName("");
      setStyle("");
      setFabric("");
      setColor("");
      setPictureUrl("");
      setLocation("");
    }
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new hat</h1>
          <form onSubmit={handleSubmit} id="create-hat-form">
            <div className="form-floating mb-3">
              <input
                onChange={handleName}
                value={name}
                placeholder="Hat name"
                required
                type="text"
                name="name"
                id="name"
                className="form-control"
              />
              <label htmlFor="name">Hat name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleStyle}
                value={style}
                placeholder="Style"
                required
                type="text"
                name="style"
                id="style"
                className="form-control"
              />
              <label htmlFor="style">Style</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleFabric}
                value={fabric}
                placeholder="Fabric"
                required
                type="text"
                name="fabric"
                id="fabric"
                className="form-control"
              />
              <label htmlFor="fabric">Fabric</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleColor}
                value={color}
                placeholder="Color"
                required
                type="text"
                name="color"
                id="color"
                className="form-control"
              />
              <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handlePictureUrl}
                value={pictureUrl}
                placeholder="Picture URL"
                required
                type="text"
                name="picture_url"
                id="picture_url"
                className="form-control"
              />
              <label htmlFor="picture_url">Picture_url</label>
            </div>
            <div className="mb-3">
              <select
                onChange={handleLocation}
                value={location}
                required
                name="location"
                id="location"
                className="form-select"
              >
                <option value="">Choose a closet location</option>
                {locations.map((location) => {
                  return (
                    <option
                      key={location.import_href}
                      value={location.import_href}
                    >
                      {location.closet_name}
                    </option>
                  );
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default HatForm;
