import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import HatsPage from "./HatsPage";
import HatForm from "./HatForm";
import ShoesList from "./ShoesList";
import ShoeForm from "./ShoesForm";

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <Routes>
        <Route path="shoes">
          <Route index element={<ShoesList />} />
          <Route path="new" element={<ShoeForm />} />
        </Route>
        <Route path="/" element={<MainPage />} />
        <Route path="/hats" element={<HatsPage />}></Route>
        <Route path="/hats/new" element={<HatForm />}></Route>
      </Routes>
    </BrowserRouter>
  );
}
export default App;
