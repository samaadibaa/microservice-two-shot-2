import React from "react";
import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import * as bootstrap from "bootstrap";
import { useParams } from "react-router-dom";

function HatColumn(props) {
  return (
    <div className="col">
      {props.list.map((data) => {
        const hat = data.hat;
        return (
          <div key={hat.href} className="card mb-3 shadow">
            <img src={hat.picture_url} className="card-img-top" />
            <div className="card-body">
              <h5 className="card-title">{hat.name}</h5>
              <button
                type="button"
                className="btn btn-secondary"
                data-bs-container="body"
                data-bs-toggle="popover"
                data-bs-placement="top"
                data-bs-content={`
                Color: ${hat.color} |
                Style: ${hat.style} |
                Fabric: ${hat.fabric} |
                Location: ${hat.location.closet_name}
                `}
              >
                View Details
              </button>
              <button
                onClick={() => DeleteHat(hat.id)}
                type="button"
                className="btn btn-danger"
                style={{ marginLeft: "10rem" }}
              >
                Delete
              </button>
            </div>
          </div>
        );
      })}
    </div>
  );
}

// DELETE an item
async function DeleteHat(id) {
  const url = `http://localhost:8090/api/hats/${id}`;
  const fetchConfig = {
    method: "DELETE",
  };

  const response = await fetch(url, fetchConfig);

  if (!response.ok) {
    console.log("Unable to delete");
  }
  window.location.reload();
}

function HatsPage(props) {
  const [hatColumns, setHatColumns] = useState([[], [], []]);

  const fetchHats = async () => {
    const url = "http://localhost:8090/api/hats/";

    try {
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();

        const requests = [];
        for (let hat of data.hats) {
          const detailUrl = `http://localhost:8090${hat.href}`;
          requests.push(fetch(detailUrl));
        }

        const responses = await Promise.all(requests);

        const columns = [[], [], []];

        let i = 0;
        for (const hatResponse of responses) {
          if (hatResponse.ok) {
            const details = await hatResponse.json();
            columns[i].push(details);
            i = i + 1;
            if (i > 2) {
              i = 0;
            }
          } else {
            console.error(hatResponse);
          }
        }
        setHatColumns(columns);
      }
    } catch (e) {
      console.error(e);
    }
  };

  useEffect(() => {
    fetchHats();
  }, []);

  // initializing popover
  const popoverTriggerList = document.querySelectorAll(
    '[data-bs-toggle="popover"]'
  );
  const popoverList = [...popoverTriggerList].map(
    (popoverTriggerEl) => new bootstrap.Popover(popoverTriggerEl)
  );

  return (
    <>
      <div>
        <h1>All Hats</h1>
        <Link
          to="/hats/new"
          className="btn btn-dark"
          style={{ marginBottom: "3rem" }}
        >
          Add a new hat
        </Link>
      </div>
      <div className="container">
        <div className="row">
          {hatColumns.map((hatList, index) => {
            return <HatColumn key={index} list={hatList} />;
          })}
        </div>
      </div>
    </>
  );
}

export default HatsPage;
