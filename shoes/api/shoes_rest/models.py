from django.urls import reverse
from django.db import models



class BinVO(models.Model):

    import_href = models.CharField(max_length=200, unique=True)
    name = models.CharField(max_length=200)

    def str(self):
        return self.name

class Shoe(models.Model):
    manufacturer = models.CharField(max_length=100)
    model_name = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    picture_url = models.URLField(null=True, blank=True)

    bin = models.ForeignKey(
        BinVO,
        related_name="bin",
        on_delete=models.CASCADE,
    )

    def get_api_url(self):
        return reverse("api_show_bin", kwargs={"pk": self.pk})
    def str(self):
        return self.model_name
