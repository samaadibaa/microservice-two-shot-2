from .models import Shoe
from django.contrib import admin


@admin.register(Shoe)
class ShoeAdmin(admin.ModelAdmin):
    pass
